# generate all the scripts that need to be run

import os
import json
import re
import glob
from plyfile import PlyData, PlyElement

BASE = "/home/mprat/Documents/data/depth_quality/"

SKIP = set(["testing_old", "realsense_d435_baseline_back", "realsense_d435_back2", "realsense_435_back"])
FNAME_SKIPS = set(["color_calib", "aruco", "rejected", "_2K"])

experiments = []

for experiment in os.listdir(BASE):
    if experiment not in SKIP:
        # print(experiment)

        for camera in os.listdir(os.path.join(BASE, experiment)):
            if camera not in SKIP:
                # print(camera)

                pngs = glob.glob(os.path.join(BASE, experiment, camera, "*.png"))
                pngs = [png for png in pngs if sum([f in png for f in FNAME_SKIPS]) == 0]
                if "realsense" in camera:
                    # remove  some

                    # generate all the script files to run
                    for png in pngs:
                        dat = png.replace(".png", ".dat")
                        ply = png.replace(".png", ".ply")

                        test_name = re.search("test[0-9]", png).group(0)

                        cmd = "python align_pointcloud_and_reference.py --rgb {} --pointcloud {} --ply {}".format(png, dat, ply)
                        experiments.append({
                            "cmd": cmd,
                            "dat": dat,
                            "ply": ply,
                            "png": png,
                            "out_ply": ply.replace(".ply", "_trimmed.ply"),
                            "test_name": test_name,
                            "camera": camera,
                            "experiment": experiment
                        })
                elif "zed" in camera:
                    for test_name in ["test1", "test2", "test3"]:
                        # pick the later one
                        # and remove the earlier one
                        s = sorted([p for p in pngs if test_name in p])
                        pngs.remove(s[0])

                    # generate all the script files to run for the 720p one
                    for png in pngs:
                        dat = png.replace(".png", ".dat")
                        ply = png.replace(".png", ".ply")
                        test_name = re.search("test[0-9]", png).group(0)

                        cmd = "python align_pointcloud_and_reference.py --rgb {} --pointcloud {} --ply {}".format(png, dat, ply)
                        experiments.append({
                            "cmd": cmd,
                            "dat": dat,
                            "ply": ply,
                            "png": png,
                            "out_ply": ply.replace(".ply", "_trimmed.ply"),
                            "test_name": test_name,
                            "camera": camera,
                            "experiment": experiment
                        })

                        ply_2K = glob.glob(os.path.join(BASE, experiment, camera, "*{}*_2K.ply".format(test_name)))[0]
                        cmd = "python align_pointcloud_and_reference.py --rgb {} --pointcloud {} --ply {}".format(png, dat, ply_2K)
                        experiments.append({
                            "cmd": cmd,
                            "dat": dat,
                            "ply": ply_2K,
                            "png": png,
                            "out_ply": ply_2K.replace(".ply", "_trimmed.ply"),
                            "test_name": test_name,
                            "camera": camera + "_2K",
                            "experiment": experiment
                        })

exp2 = []
for item in experiments:
    cc_cmd = "cloudcompare.CloudCompare -NO_TIMESTAMP -SILENT -o {} -o /home/mprat/Documents/repos/depthqual-hw/GROUNDTRUTH_V02.STL -C2M_DIST -LOG_FILE {} -AUTO_SAVE ON".format(item['out_ply'], item['out_ply'].replace(".ply", ".log"))
    item_copy = item.copy()
    item_copy['cloudcompare_cmd'] = cc_cmd
    exp2.append(item_copy)

experiments = exp2

# save the experiments file to JSON
json.dump(experiments, open("20181024-analysis.json", "w"), indent=4)
print("Num experiments: {}".format(len(experiments)))

# now actually do the rectification
for item in experiments:
    cmd = item["cmd"]
    print(cmd)
    if not os.path.isfile(item['out_ply']):
        os.system(cmd)

    # cc_cmd = item["cloudcompare_cmd"]
    # print(cc_cmd)
    # os.system(cc_cmd)

# now let's extract all the information from all the .log files
exp2 = []
for item in experiments:
    out_ply = item['out_ply']
    log = out_ply.replace(".ply", ".log")
    try:
        with open(log) as f:
            content = f.readlines()
    except FileNotFoundError:
        print("ERROR NEED TO RERUN LOG NOT FOUND {}".format(item['cloudcompare_cmd']))
        os.system(item['cloudcompare_cmd'])
        continue

    try:
        stats = [l.strip() for l in content if 'Mean' in l][0]
    except IndexError:
        print("ERROR NEED TO RERUN LOG DOESN'T HAVE THE RIGHT STUFF {}".format(item['cloudcompare_cmd']))
        os.system(item['cloudcompare_cmd'])
        continue
    nums = re.findall("[.0-9]+", stats)
    mean = nums[-2]
    std = nums[-1]
    item_copy = item.copy()
    item_copy['mean'] = mean
    item_copy['std'] = std

    # see how many points are in the PLY file
    ply = PlyData.read(item['out_ply'])
    item_copy['num_pts'] = ply['vertex'].count
    exp2.append(item_copy)

# save the results
json.dump(exp2, open("20181024-results-mean-std.json", "w"), indent=4)
print("Num experiments: {}".format(len(exp2)))
