#!/usr/bin/env python3

# align_pointcloud_and_reference.py
# ------------------------------------------------------------------------------
#
# Author: Michele Pratusevich
# Date:   10/17/2018
#
# Description: Given a target pointcloud from a camera, align it with the
#              reference coordinate frame of the depth quality fixture.
# ------------------------------------------------------------------------------
#                 (c) 2018 Root AI, Inc. - All Rights Reserved
# ------------------------------------------------------------------------------

import cv2
import numpy as np
import argparse
import transformations as tfms
from plyfile import PlyData, PlyElement


# ground truth coordinates
# the tags are 35 mm by 35 mm
# these units are in mm
GROUND_TRUTH_COORDS = {
    101: {
        "top_left": [262.3, 6, -262.3],
        "top_right": [262.3, 6, -297.3],
        "bottom_right": [297.3, 6, -297.3],
        "bottom_left": [297.3, 6, -262.3]
    },
    104: {
        "top_left": [48.5, 6, -48.5],
        "top_right": [48.5, 6, -13.5],
        "bottom_right": [13.5, 6, -13.5],
        "bottom_left": [13.5, 6, -48.5]
    },
    109: {
        "top_left": [6, 48.5, -13.5],
        "top_right": [6, 48.5, -48.5],
        "bottom_right": [6, 13.5, -48.5],
        "bottom_left": [6, 13.5, -13.5]
    },
    111: {
        "top_left": [48.5, 48.5, -6],
        "top_right": [13.5, 48.5, -6],
        "bottom_right": [13.5, 13.5, -6],
        "bottom_left": [48.5, 13.5, -6]
    },
    113: {
        "top_left": [6, 262.3, -297.3],
        "top_right": [6, 262.3, -262.3],
        "bottom_right": [6, 297.3, -262.3],
        "bottom_left": [6, 297.3, -297.3]
    },
    114: {
        "top_left": [48.5, 297.3, -6],
        "top_right": [13.5, 297.3, -6],
        "bottom_right": [13.5, 262.3, -6],
        "bottom_left": [48.5, 262.3, -6]
    },
    115: {  # 6 mm shorter in x
        "top_left": [297.3, 297.3, -6],
        "top_right": [256.3, 297.3, -6],
        "bottom_right": [256.3, 262.3, -6],
        "bottom_left": [297.3, 262.3, -6]
    },
    120: {   # 6 mm shoter in z
        "top_left": [6, 48.5, -256.3],
        "top_right": [6, 48.5, -291.3],
        "bottom_right": [6, 13.5, -291.3],
        "bottom_left": [6, 13.5, -256.3]
    },
}
COORD_NAMES = ["top_left", "top_right", "bottom_right", "bottom_left"]

TRIM_CENTER = [0.1524, 0.1524, -0.1524]
TRIM_WIDTH = [0.3048, 0.3048, 0.3048]


def is_inside_box(x, y, z):
    """
    @brief      Return true / false whether the point is inside the trim box

    @param      x     { parameter_description }
    @param      y     { parameter_description }
    @param      z     { parameter_description }

    @return     True if inside box, False otherwise.
    """
    x_min = TRIM_CENTER[0] - TRIM_WIDTH[0] / 2
    y_min = TRIM_CENTER[1] - TRIM_WIDTH[1] / 2
    z_min = TRIM_CENTER[2] - TRIM_WIDTH[2] / 2

    x_max = TRIM_CENTER[0] + TRIM_WIDTH[0] / 2
    y_max = TRIM_CENTER[1] + TRIM_WIDTH[1] / 2
    z_max = TRIM_CENTER[2] + TRIM_WIDTH[2] / 2

    return x_min <= x <= x_max and y_min <= y <= y_max and z_min <= z <= z_max


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--rgb", type=str, help="path to RGB image", required=True)
    parser.add_argument(
        "--ply", type=str, help="path to ply", required=True)
    parser.add_argument(
        "--pointcloud", type=str, help="path to pointcloud", required=True)

    args = parser.parse_args()

    print("Loading RGB image")
    color = cv2.imread(args.rgb)

    # read the memmaped .dat pointcloud file, since we need that for
    # calibration targets to know which depth value corresponds
    # to the aruco tags
    print("loading pointcloud")
    pointcloud = np.memmap(args.pointcloud, dtype=np.float16, mode='r')
    pointcloud = pointcloud.reshape(color.shape)

    # the PLY files saved from librealsense are JUST vertices (no faces)
    # so they are pretty easy to manipulate
    print("Loading .ply file")
    ply = PlyData.read(args.ply)

    # detect the aruco tags
    print("Detecting arucos")
    aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_5X5_1000)
    params = cv2.aruco.DetectorParameters_create()
    params.perspectiveRemovePixelPerCell = 10
    params.perspectiveRemoveIgnoredMarginPerCell = 0.1
    params.cornerRefinementMethod = cv2.aruco.CORNER_REFINE_CONTOUR
    marker_corners, marker_ids, rejected = cv2.aruco.detectMarkers(
        color, aruco_dict, parameters=params)

    print("Detected markers: {}".format(marker_ids))
    # draw the detected markers
    detected = cv2.aruco.drawDetectedMarkers(
        color.copy(), marker_corners, marker_ids)
    cv2.imwrite(args.rgb.replace(".png", "_arucos.png"), detected)
    rej = cv2.aruco.drawDetectedMarkers(color.copy(), rejected)
    cv2.imwrite(args.rgb.replace(".png", "_rejected.png"), rej)

    detected_arucos = {}
    # compute aruco points in world coordinates for each tag
    # we know each tag is unique, and the idea of false positives
    # is not very likely
    for corners, aruco_id in zip(marker_corners, marker_ids):
        data = {}
        # each corner is returned in top-left, top-right,
        # bottom-right, bottom-left order
        # top-left
        for index, (name, corner) in enumerate(
                zip(COORD_NAMES,
                    corners[0])):
            data[name] = {
                'raw': corner,
                'world': np.array(
                    pointcloud[int(corner[1]), int(corner[0])]).tolist()
            }
        detected_arucos[aruco_id[0]] = data.copy()

    # now we need to align the matrices to determine the transformation

    measured_coords = []
    reference_coords = []
    for tag_id, data in detected_arucos.items():
        truth = GROUND_TRUTH_COORDS[tag_id]
        for coord_name in COORD_NAMES:
            measured_coords.append(data[coord_name]['world'])
            reference_coords.append(truth[coord_name])

    # estimate the transformation matrix that moves the "given"
    # position into the reference position
    # convert the reference coordinates to mm
    print("Computing transformation")
    v0 = np.array(measured_coords)
    v1 = np.array(reference_coords) / 1000.0
    # import pudb; pudb.set_trace()
    # confidence, calib_matrix, inliers = cv2.estimateAffine3D(v0, v1)

    # if confidence < 0.99:
    #     raise RuntimeError(
    #         "confidence for calibration is {}".format(confidence))

    # rely on rigid transformation
    calib_matrix = tfms.affine_matrix_from_points(
        v0.T, v1.T, shear=False, scale=False)
    calib_matrix = calib_matrix[:3, :]

    print("Calib matrix:\n{}".format(calib_matrix))

    # transform the target pointcloud into reference coordinates
    # and write a new PLY file
    print("Transforming PLY file")
    x = ply['vertex'].data['x']
    y = ply['vertex'].data['y']
    z = ply['vertex'].data['z']
    r = ply['vertex'].data['red']
    g = ply['vertex'].data['green']
    b = ply['vertex'].data['blue']

    ply_world_points = np.vstack((x, y, z))

    # transform
    transformed_ply_world = (calib_matrix[:, :3] @ ply_world_points) + \
        calib_matrix[:, 3][:, np.newaxis]

    # write the PLY now
    x_new = transformed_ply_world[0, :]
    y_new = transformed_ply_world[1, :]
    z_new = transformed_ply_world[2, :]
    new_ply = np.array([
        (x_new[i], y_new[i], z_new[i],
            r[i], g[i], b[i]) for i in range(x.shape[0])],
        dtype=[('x', 'f4'), ('y', 'f4'), ('z', 'f4'),
               ('red', 'uint8'), ('green', 'uint8'), ('blue', 'uint8')])

    el = PlyElement.describe(new_ply, 'vertex')
    PlyData([el]).write(args.ply.replace(".ply", "_transformed.ply"))

    # now let's trim the point cloud so that nothing outside the box
    new_ply_trimmed = np.array([
        (x_new[i], y_new[i], z_new[i],
            r[i], g[i], b[i]) for i in range(x.shape[0]) if
        is_inside_box(x_new[i], y_new[i], z_new[i])],
        dtype=[('x', 'f4'), ('y', 'f4'), ('z', 'f4'),
               ('red', 'uint8'), ('green', 'uint8'), ('blue', 'uint8')])

    el = PlyElement.describe(new_ply_trimmed, 'vertex')
    PlyData([el]).write(args.ply.replace(".ply", "_trimmed.ply"))
