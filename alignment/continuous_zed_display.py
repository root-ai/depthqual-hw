import pyzed.camera as zcam
import pyzed.types as tp
import pyzed.defines as sl
import pyzed.core as core
import cv2
import datetime


if __name__ == '__main__':
    init = zcam.PyInitParameters()
    init.sdk_verbose = True
    init.camera_resolution = sl.PyRESOLUTION.PyRESOLUTION_HD720
    init.depth_mode = sl.PyDEPTH_MODE.PyDEPTH_MODE_PERFORMANCE
    init.coordinate_units = sl.PyUNIT.PyUNIT_MILLIMETER

    cam = zcam.PyZEDCamera()
    if not cam.is_opened():
        print("Opening ZED Camera...")
    status = cam.open(init)
    if status != tp.PyERROR_CODE.PySUCCESS:
        print(repr(status))
        exit()

    runtime = zcam.PyRuntimeParameters()
    runtime.sensing_mode = sl.PySENSING_MODE.PySENSING_MODE_STANDARD
    image_left = core.PyMat()
    depth_image = core.PyMat()
    pointcloud = core.PyMat()
    frame_index = 0

    key = ''
    while key != 113:  # for 'q' key
        err = cam.grab(runtime)
        if err == tp.PyERROR_CODE.PySUCCESS:
            start = datetime.datetime.now()
            cam.retrieve_image(image_left, sl.PyVIEW.PyVIEW_LEFT)
            cam.retrieve_image(depth_image, sl.PyVIEW.PyVIEW_DEPTH)
            cam.retrieve_measure(pointcloud, sl.PyMEASURE.PyMEASURE_XYZRGBA)
            end = datetime.datetime.now()
            frame_index += 1

            # try to detect arucos and show them
            color = image_left.get_data()[:, :, :3].copy()
            aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_5X5_1000)
            marker_corners, marker_ids, rejected = cv2.aruco.detectMarkers(
                color, aruco_dict)

            # draw the detected markers
            detected = cv2.aruco.drawDetectedMarkers(color, marker_corners, marker_ids)

            cv2.imshow("ZED", detected)
            cv2.imshow("Depth", depth_image.get_data())
            timestamp = cam.get_timestamp(
                sl.PyTIME_REFERENCE.PyTIME_REFERENCE_CURRENT)

            # show the FPS
            if (frame_index % 100 == 0):
                print("FPS = {}".format(100000.0 / (end - start).microseconds))

            key = cv2.waitKey(1)
        else:
            key = cv2.waitKey(1)

    cv2.destroyAllWindows()
    cam.close()
