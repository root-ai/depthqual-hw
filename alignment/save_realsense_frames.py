#!/usr/bin/env python3

# get_rgb_and_pointcloud_realsense.py
# ------------------------------------------------------------------------------
#
# Author: Michele Pratusevich
# Date:   10/17/2018
#
# Description: Get and save an image and (colorized) pointcloud from
#              a Realsense camera.
# ------------------------------------------------------------------------------
#                 (c) 2018 Root AI, Inc. - All Rights Reserved
# ------------------------------------------------------------------------------

import pyrealsense2 as rs
import datetime
import numpy as np
import json
import cv2
import argparse


WIDTH = 1280
HEIGHT = 720
FPS = 15


def get_now_as_string():
    """Gets the current time as a UTC timestamp

    Returns:
        string: utc timestamp
    """
    # because we are capturing so fast, we want both the seconds (%s)
    # and the microseconds (%f)
    return datetime.datetime.utcnow().strftime("%s%f")


class Realsense(object):
    def __init__(self):
        ctx = rs.context()
        devices = ctx.query_devices()
        dev = devices[0]
        pid = str(dev.get_info(rs.camera_info.product_id))
        if pid == "0B07":
            self.d435 = True
            self.d430 = False
        elif pid == "0AD3":
            self.d435 = False
            self.d430 = False
        elif pid == "0AD4":
            self.d435 = False
            self.d430 = True
        else:
            raise TypeError("pid {} is not recognized".format(pid))

        mode = rs.rs400_advanced_mode(dev)

        if not mode.is_enabled():
            print('Trying to enable advanced mode...')
            mode.toggle_advanced_mode(True)

            # At this point the device will disconnect and re-connect.
            print('Sleeping for 5 seconds...')
            time.sleep(5)

            # The 'dev' object will become invalid and we need to
            # initialize it again
            dev = self.__find_device()

            mode = rs.rs400_advanced_mode(dev)

        param_file = open("params.json", 'r')
        param_json = json.load(param_file)

        # remove the keys that are not supported on the d435
        if self.d435 or self.d430:
            del param_json['controls-depth-white-balance-auto']

        if self.d430:
            del param_json['controls-color-autoexposure-auto']
            del param_json['controls-color-autoexposure-manual']
            del param_json['controls-color-backlight-compensation']
            del param_json['controls-color-brightness']
            del param_json['controls-color-contrast']
            del param_json['controls-color-gain']
            del param_json['controls-color-gamma']
            del param_json['controls-color-hue']
            del param_json['controls-color-power-line-frequency']
            del param_json['controls-color-saturation']
            del param_json['controls-color-sharpness']
            del param_json['controls-color-white-balance-auto']
            del param_json['controls-color-white-balance-manual']

        param_str = json.dumps(param_json).replace("'", '\"')

        mode.load_json(param_str)

        self.pipeline = rs.pipeline()
        config = rs.config()

        print("Enabling streams")

        config.enable_stream(
            rs.stream.depth,
            WIDTH, HEIGHT,
            rs.format.z16,
            FPS)

        if self.d435:
            stm = rs.stream.color
        else:
            stm = rs.stream.infrared

        if self.d430:
            fmt = rs.format.y8
        else:
            fmt = rs.format.rgb8

        config.enable_stream(stm, WIDTH, HEIGHT, fmt, FPS)

        self.profile = self.pipeline.start(config)

        # alignment (only for d435)
        self.align = rs.align(rs.stream.color)

    def get_aligned_rgb_and_depth_and_save(self, prefix=""):
        # capture an RGB frame WITHOUT the laser, so we can get good readings
        # on the aruco tags, THEN turn the laser back on so we can
        # get a pointcloud with better accuracy
        depth_sensor = self.profile.get_device().first_depth_sensor()
        depth_sensor.set_option(rs.option.emitter_enabled, 0)

        # wait for 10 frames before getting another, just to let it stabilize
        for i in range(10):
            frames = self.pipeline.wait_for_frames()
            # make sure you align if you're using the d435
            if self.d435:
                frames = self.align.process(frames)

        if self.d435:
            color_frame = frames.get_color_frame()
        else:
            color_frame = frames.get_infrared_frame()
        print("Got color frame")

        # wait for 10 frames before getting another, just to let it stabilize
        for i in range(10):
            frames = self.pipeline.wait_for_frames()
            # make sure you align if you're using the d435
            if self.d435:
                frames = self.align.process(frames)

        depth_frame = frames.get_depth_frame()
        print("Got depth frame")

        if not depth_frame or not color_frame:
            print("Didn't get depth or color, so not saving anything")
            return

        now_string = get_now_as_string()

        pointcloud = rs.pointcloud()
        pointcloud.map_to(color_frame)
        pc = pointcloud.calculate(depth_frame)

        # save pointcloud
        pc.export_to_ply("{}_{}.ply".format(prefix, now_string), color_frame)

        # # get the depth scale so we can get the full units
        # # this is probably going to be 1mm
        # depth_scale = self.profile.get_device().first_depth_sensor().get_depth_scale()  # noqa
        # print("depth_scale: {}".format(depth_scale))

        # save color frame
        color = np.asanyarray(color_frame.get_data())
        # we need to save this as BGR because REASONS
        if self.d430:
            cv2.imwrite(
                "{}_{}.png".format(prefix, now_string),
                cv2.cvtColor(color, cv2.COLOR_GRAY2BGR))
        else:
            cv2.imwrite(
                "{}_{}.png".format(prefix, now_string),
                cv2.cvtColor(color, cv2.COLOR_RGB2BGR))

        # save raw pointcloud (with zeros) for calibration
        # just save it as a memmap array
        pts = np.reshape(np.array(pc.get_vertices()),
                                 (HEIGHT, WIDTH))
        point_array = np.dstack((pts['f0'], pts['f1'], pts['f2']))
        fp = np.memmap(
            "{}_{}.dat".format(prefix, now_string), dtype=np.float16,
            mode='w+', shape=point_array.shape)
        fp[:] = point_array
        # make sure you actually write it to disk
        fp.flush()

        self.pipeline.stop()
        print("Saved to {}".format(now_string))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--prefix", type=str, help="save_prefix")
    args = parser.parse_args()

    prefix = args.prefix

    # capture an RGB and a pointcloud from a realsense camera
    cam = Realsense()
    cam.get_aligned_rgb_and_depth_and_save(prefix)
