import pyzed.camera as zcam
import pyzed.types as tp
import pyzed.defines as sl
import pyzed.core as core
import cv2
import numpy as np
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--prefix", type=str, help="save_prefix")
    args = parser.parse_args()

    prefix = args.prefix
    """
    First capture at the initial resolution, so we get all the information we
    need for alignment.
    """
    init = zcam.PyInitParameters()
    init.sdk_verbose = True
    init.camera_resolution = sl.PyRESOLUTION.PyRESOLUTION_HD720
    init.depth_mode = sl.PyDEPTH_MODE.PyDEPTH_MODE_PERFORMANCE
    init.coordinate_units = sl.PyUNIT.PyUNIT_METER

    cam = zcam.PyZEDCamera()
    if not cam.is_opened():
        print("Opening ZED Camera...")
    status = cam.open(init)
    if status != tp.PyERROR_CODE.PySUCCESS:
        print(repr(status))
        exit()

    runtime = zcam.PyRuntimeParameters()
    runtime.sensing_mode = sl.PySENSING_MODE.PySENSING_MODE_STANDARD
    image_left = core.PyMat()
    depth_image = core.PyMat()
    pointcloud = core.PyMat()
    timestamp = None

    # wait for 5 frames before we trust the results
    for i in range(5):
        print("Grabbing frame")
        err = cam.grab(runtime)
        if err == tp.PyERROR_CODE.PySUCCESS:
            cam.retrieve_image(image_left, sl.PyVIEW.PyVIEW_LEFT)
            cam.retrieve_measure(depth_image, sl.PyMEASURE.PyMEASURE_XYZ)
            cam.retrieve_measure(pointcloud, sl.PyMEASURE.PyMEASURE_XYZRGBA)
            timestamp = cam.get_timestamp(
                sl.PyTIME_REFERENCE.PyTIME_REFERENCE_CURRENT)

        if i == 3:
            # save a backup image, just in case
            cv2.imwrite(
                "{}_{}_720p.png".format(prefix, timestamp),
                image_left.get_data())

    # we'll use the 5th one for the results we want
    cv2.imwrite(
        "{}_{}_720p.png".format(prefix, timestamp), image_left.get_data())
    # change nans to zeros
    depth_image_fixed = np.nan_to_num(depth_image.get_data()[:, :, :3])
    # save it
    fp = np.memmap(
        "{}_{}_720p.dat".format(prefix, timestamp), dtype=np.float16,
        mode='w+', shape=depth_image_fixed.shape)
    fp[:] = depth_image_fixed
    # make sure you actually write it to disk
    fp.flush()

    # save PLY
    zcam.save_mat_point_cloud_as(
        pointcloud, sl.PyPOINT_CLOUD_FORMAT.PyPOINT_CLOUD_FORMAT_PLY_ASCII,
        "{}_{}_720p.ply".format(prefix, timestamp), with_color=True)

    print("Saved images at timestamp {}".format(timestamp))
    cam.close()

    """
    Now capture at higher resolution to see if the pointcloud is better.
    """
    init.camera_resolution = sl.PyRESOLUTION.PyRESOLUTION_HD2K

    # re-open camera
    status = cam.open(init)
    if status != tp.PyERROR_CODE.PySUCCESS:
        print(repr(status))
        exit()

    # wait for 5 frames before we trust the results
    for i in range(5):
        print("Grabbing frame")
        err = cam.grab(runtime)
        if err == tp.PyERROR_CODE.PySUCCESS:
            cam.retrieve_image(image_left, sl.PyVIEW.PyVIEW_LEFT)
            cam.retrieve_measure(depth_image, sl.PyMEASURE.PyMEASURE_XYZ)
            cam.retrieve_measure(pointcloud, sl.PyMEASURE.PyMEASURE_XYZRGBA)
            timestamp = cam.get_timestamp(
                sl.PyTIME_REFERENCE.PyTIME_REFERENCE_CURRENT)

    # we'll use the 5th one for the results we want
    cv2.imwrite(
        "{}_{}_2K.png".format(prefix, timestamp), image_left.get_data())
    # change nans to zeros
    depth_image_fixed = np.nan_to_num(depth_image.get_data()[:, :, :3])
    # save it
    fp = np.memmap(
        "{}_{}_2K.dat".format(prefix, timestamp), dtype=np.float16,
        mode='w+', shape=depth_image_fixed.shape)
    fp[:] = depth_image_fixed
    # make sure you actually write it to disk
    fp.flush()

    # save PLY
    zcam.save_mat_point_cloud_as(
        pointcloud, sl.PyPOINT_CLOUD_FORMAT.PyPOINT_CLOUD_FORMAT_PLY_ASCII,
        "{}_{}_2K.ply".format(prefix, timestamp), with_color=True)

    print("Saved images at timestamp {}".format(timestamp))
    cam.close()
