#!/usr/bin/env python3

# get_rgb_and_pointcloud_realsense.py
# ------------------------------------------------------------------------------
#
# Author: Michele Pratusevich
# Date:   10/17/2018
#
# Description: Get and save an image and (colorized) pointcloud from
#              a Realsense camera.
# ------------------------------------------------------------------------------
#                 (c) 2018 Root AI, Inc. - All Rights Reserved
# ------------------------------------------------------------------------------

import pyrealsense2 as rs
import numpy as np
import json
import cv2


WIDTH = 1280
HEIGHT = 720
FPS = 15


class Realsense(object):
    def __init__(self):
        ctx = rs.context()
        devices = ctx.query_devices()
        dev = devices[0]
        pid = str(dev.get_info(rs.camera_info.product_id))
        if pid == "0B07":
            self.d435 = True
            self.d430 = False
        elif pid == "0AD3":
            self.d435 = False
            self.d430 = False
        elif pid == "0AD4":
            self.d435 = False
            self.d430 = True
        else:
            raise TypeError("pid {} is not recognized".format(pid))

        mode = rs.rs400_advanced_mode(dev)

        if not mode.is_enabled():
            print('Trying to enable advanced mode...')
            mode.toggle_advanced_mode(True)

            # At this point the device will disconnect and re-connect.
            print('Sleeping for 5 seconds...')
            time.sleep(5)

            # The 'dev' object will become invalid and we need to
            # initialize it again
            dev = self.__find_device()

            mode = rs.rs400_advanced_mode(dev)

        param_file = open("params.json", 'r')
        param_json = json.load(param_file)

        # remove the keys that are not supported on the d435
        if self.d435 or self.d430:
            del param_json['controls-depth-white-balance-auto']

        if self.d430:
            del param_json['controls-color-autoexposure-auto']
            del param_json['controls-color-autoexposure-manual']
            del param_json['controls-color-backlight-compensation']
            del param_json['controls-color-brightness']
            del param_json['controls-color-contrast']
            del param_json['controls-color-gain']
            del param_json['controls-color-gamma']
            del param_json['controls-color-hue']
            del param_json['controls-color-power-line-frequency']
            del param_json['controls-color-saturation']
            del param_json['controls-color-sharpness']
            del param_json['controls-color-white-balance-auto']
            del param_json['controls-color-white-balance-manual']

        param_str = json.dumps(param_json).replace("'", '\"')

        mode.load_json(param_str)

        self.pipeline = rs.pipeline()
        config = rs.config()

        print("Enabling streams")

        config.enable_stream(
            rs.stream.depth,
            WIDTH, HEIGHT,
            rs.format.z16,
            FPS)

        if self.d435:
            stm = rs.stream.color
        else:
            stm = rs.stream.infrared

        if self.d430:
            fmt = rs.format.y8
        else:
            fmt = rs.format.rgb8

        config.enable_stream(stm, WIDTH, HEIGHT, fmt, FPS)

        self.profile = self.pipeline.start(config)


if __name__ == '__main__':
    # capture an RGB and a pointcloud from a realsense camera
    cam = Realsense()

    frame_index = 0
    key = ''
    while key != 113:  # for 'q' key
        frames = cam.pipeline.wait_for_frames()

        if cam.d435:
            color_frame = frames.get_color_frame()
        else:
            color_frame = frames.get_infrared_frame()
        color = np.asanyarray(color_frame.get_data())

        aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_5X5_1000)
        marker_corners, marker_ids, rejected = cv2.aruco.detectMarkers(
            color, aruco_dict)

        # draw the detected markers
        detected = cv2.aruco.drawDetectedMarkers(
            color, marker_corners, marker_ids)

        if cam.d430:
            cv2.imshow("RS", cv2.cvtColor(detected, cv2.COLOR_GRAY2BGR))
        else:
            cv2.imshow("RS", cv2.cvtColor(detected, cv2.COLOR_RGB2BGR))

        # display the exposure that the camera actually set
        frame_index += 1
        if frame_index % 10 == 0:
            depth_frame = frames.get_depth_frame()
            exp = depth_frame.get_frame_metadata(
                rs.frame_metadata_value.actual_exposure)
            print("Exposure on depth sensor: {}".format(exp))

        key = cv2.waitKey(1)

    cv2.destroyAllWindows()
