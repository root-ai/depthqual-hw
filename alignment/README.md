Installation
----

To use the Python scripts in this folder you need:

* OpenCV with contrib installed: https://root-ai.atlassian.net/wiki/spaces/TD/pages/65536205/OpenCV+with+contrib+on+Linux
* librealsense (version 2.15.0 or earlier): https://root-ai.atlassian.net/wiki/spaces/TD/pages/52002850/RealSense+Camera
* the `plyfile` pip package for manipulating PLYs: https://github.com/dranjan/python-plyfile

It assumes you are using Python 3 - if you are using Python 2, it has not been tested!
